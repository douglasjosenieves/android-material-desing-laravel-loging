package com.eajy.materialdesigndemo.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.eajy.materialdesigndemo.R;

public class WellcomeActivity extends AppCompatActivity {

    private TextView name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wellcome);
        name = findViewById(R.id.name);
        SharedPreferences prefs =
                getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);

        String p_name = prefs.getString("name", "");
        name.setText(p_name);
    }


    public void ir_a_inicio(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }
}
